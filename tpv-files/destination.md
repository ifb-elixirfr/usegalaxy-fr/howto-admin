# Description Générale

Le fichier `destination.yml` définit les destinations où les outils peuvent être exécutés, ainsi que les ressources maximales disponibles pour chaque destination.
Cela inclut des détails sur le type de runner, les limites de ressources (comme les coeurs, la mémoire et les GPU), ainsi que d’autres paramètres de configuration spécifiques aux destinations.

## 1. Définition des destinations (`destinations`)

Les destinations dans ce fichier représentent les environnements d'exécution où les outils seront programmés.
Chaque destination est associée à un runner spécifique et des limites sur les ressources qu'elle peut gérer.

```yaml
destinations:
  default:
    abstract: true
    runner: slurm

  _slurm_destination:
    abstract: true
    max_cores: 250
    max_mem: 2041
    params:
      nativeSpecification: "--cpus-per-task={cores} --mem={round(mem*1024)} --partition={partition} {additional_spec} {reservation}"

```

- `_slurm_destination` : Le nom de la destination.
- `runner` : Le type de runner utilisé pour cette destination (par exemple, `slurm`).
- `max_cores` : Le nombre maximal de coeurs que cette destination peut accepter.
- `max_mem` : La quantité maximale de mémoire (en GB) que cette destination peut accepter.
- `nativeSpecification` : Spécifie les options natives pour la soumission des tâches à Slurm. Cela inclut les coeurs par tâche (--cpus-per-task), la mémoire par tâche (--mem), la partition (--partition), ainsi que des options supplémentaires ({additional_spec}) et de réservation ({reservation})

## 2. Règles de conteneurs (`rules`)

Les règles conditionnelles configurent le comportement de la destination en fonction de la présence de conteneurs Singularity ou Docker.

*Règle pour Singularity :*

```yaml
rules:
  - id: slurm_destination_singularity_rule
    if: entity.params.get('singularity_enabled')
    params:
        require_container: true
        singularity_volumes: '$galaxy_root:ro,$tool_directory:ro,$job_directory:rw,$working_directory:rw,$default_file_path:rw,/cvmfs/data.galaxyproject.org:rw,/tmp:rw,/shared/ifbstor1/galaxy/mutable-data/tmp:rw,/shared/ifbstor1/galaxy/datasets2:rw'
        singularity_default_container_id: '/cvmfs/singularity.galaxyproject.org/all/ubuntu:20.04'
        tmp_dir: 'True'
    env:
      - name: LC_ALL
        value: C
      - name: APPTAINER_DISABLE_CACHE
        value: 'True'
      - name: APPTAINER_CACHEDIR
        value: /tmp/singularity
      - name: APPTAINER_TMPDIR
        value: /shared/home/galaxy/.singularity
```

- `if: entity.params.get('singularity_enabled')` : Cette règle s'applique si l'outil a `singularity_enabled` dans ses options.
- `require_container: true` : L'exécution nécessite l'utilisation de Singularity.
- `singularity_volumes` : Les volumes montés dans le conteneur Singularity, avec des répertoires Galaxy, des répertoires de travail, et des chemins spécifiques pour les données.
- `singularity_default_container_id` : L'image par défaut pour Singularity, ici `ubuntu:20.04`.
- `tmp_dir: 'True'` : Indique que l'utilisation d'un répertoire temporaire est activée.
- **Variables d'environnement** :
  - `LC_ALL: C` : Définit la locale pour garantir la reproductibilité.
  - `APPTAINER_DISABLE_CACHE: 'True'` : Désactive le cache pour Apptainer.
  - `APPTAINER_CACHEDIR` : Définit le répertoire du cache Singularity.
  - `APPTAINER_TMPDIR` : Définit le répertoire temporaire pour Apptainer.

*Règle pour Docker :*

```yaml
  - id: slurm_destination_docker_rule
    if: entity.params.get('docker_enabled')
    params:
        nativeSpecification: "--cpus-per-task={cores} --mem={round(mem*1024)} --partition=docker {additional_spec} {reservation}"
        require_container: true
        docker_volumes: '$defaults,/shared/ifbstor1/galaxy/datasets2:ro'
        docker_memory: '{mem}G'
        docker_sudo: false
        docker_auto_rm: true
        docker_default_container_id: busybox:ubuntu-14.04
        docker_set_user: ''
        tmp_dir: 'True'
```

- `if: entity.params.get('docker_enabled')` : Cette règle s'applique si l'outil a `docker_enabled` dans ses options.
- `require_container: true` : L'exécution nécessite l'utilisation de Docker.
- `nativeSpecification` : Spécifie la configuration des ressources pour Docker, incluant la partition (`--partition=docker`), le nombre de coeurs et la mémoire.
- `docker_volumes` : Les volumes montés dans le conteneur Docker.
- `docker_memory` : La mémoire allouée pour le conteneur.
- `docker_sudo: false` : Indique que Docker ne doit pas utiliser `sudo`.
- `docker_auto_rm: true` : Active la suppression automatique des conteneurs une fois l'exécution terminée.
- `docker_default_container_id` : L'image Docker par défaut, ici `busybox:ubuntu-14.04`.
- `docker_set_user` : Définit l'utilisateurice pour le conteneur.
- `tmp_dir: 'True'` : Indique que l'utilisation d'un répertoire temporaire est activée.

## 3. Acceptation des tags (`scheduling`)

Les tags de planification définissent les types de tâches ou de ressources acceptées par la destination.
Cela permet de spécifier les types de conteneurs ou autres systèmes de gestion des ressources supportés.
La section `scheduling` détermine comment les tâches sont planifiées et les conditions sous lesquelles elles peuvent être exécutées sur cette destination.
Elle permet de définir les ressources nécessaires et les utilisateurices autorisés à utiliser la destination.

### Exemple de configuration

```yaml
scheduling:
  require:
    - slurm-singularity-gpu
  accept:
    - admin-users
    - docker
    - singularity
```

### 1. require

La directive `require` spécifie les prérequis nécessaires pour que la destination soit disponible pour l'exécution de tâches.
Dans cet exemple, la tâche doit demander explicitement la ressource `slurm-singularity-gpu`.
Cela garantit que seules les tâches qui ont ce besoin spécifique peuvent être planifiées sur cette destination.
Tous les tags spécifiés dans `require` doivent être présents pour qu'un outil ou job soit accepté par la destination. (`ET` logique)

### 2. accept

La directive `accept` définit les ressources ou prérequis sont autorisés à soumettre des tâches vers cette destination.
Dans cet exemple,il n'y a que les utilisateurices appartenant au groupe `admin-users` peuvent soumettre des tâches.
La destination acceptera les outils ou jobs possédant au moins un des tags spécifiés dans `accept`. (`OUu` logique)

## 4. L'héritage

La directive `inherits` permet à une destination de réutiliser les configurations d'une autre destination.
Cela est utile pour définir des variantes ou des spécialisations basées sur une configuration de base existante, sans avoir à dupliquer toutes les options.
En héritant d'une destination, les paramètres définis dans celle-ci sont récupérés, et peuvent être modifiés ou étendus dans la destination enfant.

### Exemple : `slurm_singularity_gpu`

```yaml
slurm_singularity_gpu:
  inherits: _slurm_destination
  runner: slurm
  max_cores: 62
  max_mem: 515
  params:
    nativeSpecification: "--cpus-per-task={cores} --mem={round(mem*1024)} --partition=gpu --gres=gres:gpu:3g.20gb:1 --time=72:00:00"
    singularity_enabled: true
  scheduling:
    require:
      - slurm-singularity-gpu
    accept:
      - admin-users
```

- `inherits` : Permet à `slurm_singularity_gpu` de récupérer la configuration de base de la destination `_slurm_destination`. Cela signifie que tous les paramètres définis dans `_slurm_destination` (comme `max_cores`, `max_mem`, et `params`) seront automatiquement appliqués à `slurm_singularity_gpu`, sauf si des valeurs sont explicitement redéfinies dans cette destination.

## 5. Tableau récapitulatif des destinations

### Résumé des destinations dans `destination.yml`

|     Destination          |     Runner             |     Inherits         |     Params    |     Require               |     Accept            |
|--------------------------|------------------------|----------------------|---------------|---------------------------|-----------------------|
|   default                |   slurm                | -                    |         -     | -                         |  -                    |
|   _slurm_destination     |   slurm                | -                    | nativeSpecification, max_cores: 1, max_mem: 1.5 | - | docker, singularity, pulsar |
|   slurm                  |   slurm                |   _slurm_destination | Hérite des paramètres de `_slurm_destination`.  | - | -           |
| pulsar_embedded_docker_gxit |     pulsar_embedded |   _slurm_destination | docker_enabled: true, docker_net: bridge, container_monitor_result: callback | pulsar-embedded-docker-gxit  | admin-users              |
|   slurm_admin            |   slurm                |   _slurm_destination | Hérite des paramètres de `_slurm_destination`.| admin-users | -   |
|   slurm_singularity_gpu  |   slurm                |   _slurm_destination | nativeSpecification: --cpus-per-task={cores} --mem={round(mem*1024)} --partition=gpu --gres=gres:gpu:3g.20gb:1 --time=72:00:00, singularity_enabled: true | slurm-singularity-gpu  | admin-users  |
