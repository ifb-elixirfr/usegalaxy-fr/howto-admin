# Contexte général

Le fichier `users.yml` permet de définir et de gérer les utilisateurices d'un système ou d'une application.
Il contient une liste d'utilisateurices, leurs rôles et les permissions associées, ainsi que les groupes ou rôles d'héritage.
Ce fichier est structuré en plusieurs parties : les groupes d'utilisateurices, les utilisateurices individuel·le·s, et les rôles qu'iels héritent.

## Structure de `users.yml`

Le fichier `users.yml` utilise une syntaxe YAML pour organiser les données. La structure générale se compose de groupe d'utilisateurices et d'une liste d'utilisateurices individuel·le·s.

### 1. Définition du groupe d'utilisateurices

La clé `admin_users` définit un groupe d'utilisateurices ayant des privilèges administratifs.

```yaml
admin_users:
  abstract: true
  scheduling:
    require:
      - admin-users
```

- **abstract** : Définit ce groupe comme une base abstraite. Cela signifie que `admin_users` n'est pas un·e utilisateurice réel·le, mais un modèle que d'autres utilisateurices peuvent hériter.
- **scheduling** : Ce sous-ensemble contient des informations liées à la gestion du planificateur, avec ici la clé `require` indiquant que les utilisateurices de ce groupe doivent avoir le rôle `admin-users` pour certaines tâches programmées.

### 2. utilisateurices individuel·le·s

Les utilisateurices sont défini·e·s avec leurs adresses e-mail ou une expression régulière pour les correspondances multiples, et iels peuvent hériter du groupe `admin_users`. Par exemple :

```yaml
  abromics-support@groupes.france-bioinformatique.fr:
    inherits: admin_users
```

- **adresses e-mail** : Chaque ligne représente un·e utilisateurice, identifié·e par son adresse e-mail.
- **inherits** : Définit les rôles dont l'utilisateurice hérite. Par exemple, `abromics-support@groupes.france-bioinformatique.fr` hérite des privilèges du groupe `admin_users`.

### 3. Regex d'utilisateurice

```yaml
  .*abromics.*france-bioinformatique.fr:
    inherits: admin_users
```

Cet exemple utilise une expression régulière (`.*abromics.*france-bioinformatique.fr`) pour inclure tous les utilisateurices dont l'adresse e-mail contient `abromics` et `france-bioinformatique.fr`. Ils héritent tous des privilèges du groupe `admin_users`.
