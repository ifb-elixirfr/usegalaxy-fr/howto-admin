# Description Générale

Le fichier `interactive_tools.yml` est utilisé pour définir et configurer les outils interactifs dans Galaxy. Il contient des informations spécifiques sur les ressources nécessaires (comme les coeurs, la mémoire, et les paramètres de conteneur Docker) pour chaque outil interactif, ainsi que des règles et conditions d'exécution. Ces outils permettent aux utilisateurices d'interagir avec leurs données via des interfaces graphiques, comme des notebooks Jupyter ou des applications similaires, qui s'ouvrent dans un nouvel onglet du navigateur

## 1. interactive_tool par défaut

```yaml
tools:
  interactive_tool:
    cores: 1
    mem: 4
    params:
      docker_enabled: true
    scheduling:
      require:
        - pulsar-embedded-docker-gxit
    rules:
      - if: not user
        fail: |
          Interactive tools require registration. Please log-in or register on https://usegalaxy.fr/login
```

- `interactive_tool` est le nom de l'outil interactif par défaut qui alloue des ressources spécifiques pour les sessions interactives sur Galaxy.
- Chaque interactive tool est configuré pour utiliser **1 coeur** et **4 Go de mémoire** par défaut.
- Le paramètre `docker_enabled` est activé, indiquant que l'outil doit être exécuté dans un conteneur Docker.
- `scheduling` :
  - L'outil a le profil de ressources `pulsar-embedded-docker-gxit`.
- `rules` :
  - Si l'utilisateurice n'est pas connecté·e (`not user`), l'exécution échoue et l'utilisateurice est invité·e à se connecter ou à s'inscrire.

## 2. interactive tool classique

### interactive_tool_askomics

```yaml
  interactive_tool_askomics:
    inherits: interactive_tool
```

#### Description

- `interactive_tool_askomics` hérite de la configuration de `interactive_tool`, ce qui signifie qu'il partage les mêmes paramètres de ressources, d'utilisation de Docker et de planification.
- Il est donc configuré pour fonctionner de la même manière que `interactive_tool`, mais sous un autre nom ou contexte d'utilisation (en l'occurrence **interactive_tool_askomics**).

## 3. Jupyter notebook & Rstudio

- Suite à des abus nous avons dû mettre en place un controle plus strict pour les utilisateurices de ces outils.
  
```yaml
  interactive_tool_jupyter_notebook:
    inherits: interactive_tool
    rules:
      - if: not any(r.name == "gxit-users" for r in user.all_roles() if not r.deleted)
        fail: |
          Due to abuse, Jupyter usage requires specific permissions from administrators. Please ask us on https://community.france-bioinformatique.fr
```

- **Est ce que l'utilisateurice a le role  `gxit-users`

  - Avant d'exécuter l'outil, il est vérifié que l'utilisateurice appartient au groupe `gxit-users` de galaxy. Si ce n'est pas le cas, l'exécution échoue et un message d'erreur est renvoyé. Iel est invité·e à demander cette permission via le forum [France Bioinformatique](https://community.france-bioinformatique.fr).
  - Il faudra alors ajouter dans usegalaxy.fr --> admin --> user management --> sélectionner l'utilisateurice --> manage Roles and Groups --> ajouter le role `gxit-users` à l'utilisateurice.
