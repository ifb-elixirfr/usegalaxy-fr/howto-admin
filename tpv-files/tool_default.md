# Description Générale

Le fichier `tool_default.yml` définit les paramètres par défaut pour les outils, les règles d'exécution, et les comportements spécifiques associés à l'exécution des jobs.

## 1. Global

La section `global` définit les paramètres hérités par défaut pour tous les outils.

```yaml
global:
  default_inherits: default
```

- `default_inherits` :  définit sur `default`, ce qui signifie que toutes les configurations des outils hériteront de la configuration `default`.

## 2. Tools

Cette section définit les paramètres par défaut applicables aux outils non-définis.
Il est ici question de la configuration par `default`.

```yaml
tools:
  default:
    context:
      partition: fast
      additional_spec: ''
      reservation: ''
    cores: 1
    mem: 2
    gpus: 0
```

### 1. context

Les éléments du contexte définissent des paramètres relatifs à la planification des tâches sur des ressources spécifiques :

- `partition` : La partition Slurm utilisée pour exécuter les jobs. Par défaut, on utilise la partition `fast`.
- `additional_spec` : Paramètres supplémentaires qui peuvent être ajoutés au job, laissée vide par défaut.
- `reservation` : Réservation spécifique pour un job, laissée vide par défaut.

### 2. cores, mem, gpus

Ces valeurs définissent les ressources par défaut pour les jobs :

- `cores` : Nombre de coeurs alloués par défaut. Ici, un seul coeur.
- `mem` : Quantité de mémoire par défaut, définie sur 2 Go.
- `gpus` : Nombre de GPU par défaut, ici défini à 0.

### 3. params

Les paramètres supplémentaires qui peuvent être utilisés dans la configuration.

### 4. rules

Les règles permettent de personnaliser l'exécution des jobs en fonction des paramètres du contexte ou d'autres conditions.
Chaque règle est conditionnelle, définie par un test (`if`), suivi d'un bloc d'exécution (`execute`).

- **Règle pour le TIaaS**

```yaml
rules:
  - if: user is not None
    execute: |
      import random
      user_roles = [r.name for r in user.all_roles() if not r.deleted and r.name.startswith("training-")]
      entity.context['reservation'] = ' --reservation=%s ' % (random.choice(user_roles)) if user_roles else ''
```

- `if` : Si l'utilisateurice est connecté·e.
- `execute` : Si des rôles d'utilisateurice commencent par "training-", une réservation aléatoire commençant par `training-` est assignée au job.
  
### 5. resubmit

Cette section définit les conditions sous lesquelles un job peut être réexécuté en cas d'échec, avec des ajustements possibles (comme la mémoire). Par exemple :

```yaml
resubmit:
  with_more_mem_on_failure:
    condition: memory_limit_reached and attempt <= 3
    destination: tpv_dispatcher
    delay: 'attempt * 30'
```

- **condition** : Spécifie la condition pour réessayer, ici si la limite de mémoire est atteinte et que le nombre de tentatives est inférieur ou égal à 3.
- **destination** : Destination vers laquelle le job est réenvoyé (`tpv_dispatcher`).
- **delay** : Délai entre chaque tentative, calculé en fonction du nombre de tentatives (`attempt * 30`).

## 2. Ressoumission des jobs

### 1. Règle d'exécution avec `job.destination_params`

```yaml
- if: job.destination_params
  execute: |
    entity.mem = float(job.destination_params.get('tpv_mem', 2)) * int(job.destination_params.get('SCALING_FACTOR', 2))
```

- `job.destination_params` est défini après la première exécution du job. La mémoire (`entity.mem`) est appliquée uniquement lors de la resubmission. Elle utilise `tpv_mem` (mémoire utilisée lors de la première exécution) multipliée par `SCALING_FACTOR` (facteur de mise à l'échelle).
- À la première exécution de l'outil, soit il utilisera les paramètres par défaut, soit les paramètres définis dans tools.yml.

### 2. Paramètre `SCALING_FACTOR`

```yaml
params:
  SCALING_FACTOR: "{2 * int(job.destination_params.get('SCALING_FACTOR', 2)) if job.destination_params else 2}"
```

- `SCALING_FACTOR` est calculé en fonction de `job.destination_params` après la première exécution. Si non défini, la valeur par défaut est `2`.
- Il n'est pas utilisé lors de la première exécution de l'outil.

### 3. Stratégie de resubmission

```yaml
resubmit:
  with_more_mem_on_failure:
    condition: memory_limit_reached and attempt <= 3
    destination: tpv_dispatcher
    delay: 'attempt * 30'
```

- Si la limite de mémoire est atteinte et le nombre de tentatives est inférieur ou égal à 3, le job est réexécuté avec plus de mémoire (`entity.mem` est recalculée) et un délai entre les tentatives (multiplié par `attempt * 30` secondes).
- /!\ actuellement (release_24.1) cette option ne fonctionne pas, on peut resoumettre un jobs qu'une seule fois
