# Description Générale

Le fichier `tools.yml` permet de spécifier des paramètres par défaut pour l'exécution des outils, y compris les ressources (nombre de coeurs, mémoire), des paramètres spécifiques à chaque outil, des spécifications de conteneurs (comme Singularity ou Docker), ainsi que des paramètres liés à l'environnement d'exécution.

## 1. Structure générale

**Paramètres par défaut pour les outils :**

- Des valeurs de **cores** et **mem** sont définies pour des outils spécifiques.

     ```yaml
     upload1:
       cores: 1
       mem: 0.3
     ```

**Outils génériques et outils du toolshed :**

- Chaque outil référencé dans le [`toolshed`](https://toolshed.g2.bx.psu.edu/) peut être configuré individuellement pour utiliser des ressources différentes.
- Les outils sont associés à des spécifications de ressources et peuvent être configurés pour utiliser **Singularity** ou d'autres options de conteneurisation.

     ```yaml
     toolshed.g2.bx.psu.edu/repos/genouest/braker3/braker3/3.0.3.*:
       cores: 6
       mem: 16
       params:
         singularity_enabled: true
         singularity_container_id_override: docker://teambraker/braker3:v.1.0.4
     ```

1. **Partition et spécifications supplémentaires :**
   - Certains outils ont des spécifications supplémentaires comme la **partition** Slurm (par exemple, `partition: long`) et des arguments supplémentaires pour l'exécution (`additional_spec: --time=48:00:00`).

     ```yaml
     toolshed.g2.bx.psu.edu/repos/frogs/frogs/FROGS_affiliation_OTU/.*:
       cores: 7
       mem: 35
       context:
         partition: long
         additional_spec: --time=48:00:00
     ```

2. **Règles basées sur la taille des entrées :**
   - Pour certains outils, des **règles conditionnelles** sont définies en fonction de la taille d'entrée (`input_size`). Cela permet d'ajuster dynamiquement les ressources allouées en fonction de la taille des données traitées.
   - Exemple :

     ```yaml
     toolshed.g2.bx.psu.edu/repos/devteam/bwa/bwa_mem/.*:
       cores: 28
       mem: 128
       rules:
         - if: input_size < 0.25
           cores: 2
           mem: 8
         - if: 0.25 <= input_size < 1
           cores: 4
           mem: 16
         - if: 1 <= input_size < 20
           cores: 8
           mem: 31
     ```

3. **Conteneurs et autres configurations spécifiques :**
   - Pour certains outils, des paramètres spécifiques à Singularity ou Docker sont définis pour permettre leur exécution dans des environnements conteneurisés.

     ```yaml
     toolshed.g2.bx.psu.edu/repos/galaxy-australia/alphafold2/alphafold/.*:
       cores: 10
       mem: 69
       params:
         singularity_enabled: true
         singularity_run_extra_arguments: "--env ALPHAFOLD_DB=/shared/bank/alphafold2/current"
     ```

## 2. Les ressources

Dans une configuration Slurm, les partitions sont des groupes de noeuds de calcul qui peuvent être alloués à des tâches. Chaque partition peut avoir des restrictions de temps associées pour les tâches qui y sont soumises.

- La partition détermine les noeuds qui seront utilisés pour exécuter un travail.
- Le temps (indiqué par --time) détermine la durée maximale allouée pour un travail sur cette partition.
- Les ressources ne peuvent pas dépasser les valeurs suivantes.

| Partitions | Time out    | Max resources / user |
| ---------- | ----------- | -------------------- |
| fast       | <= 24 hours | cpu=300, mem=1500GB  |
| long       | <= 30 days  | cpu=300, mem=1500GB  |
| bigmem     | <= 60 days  | mem=4000GB           |
| gpu        | <= 3 days   | cpu=300, mem=1500GB  |
