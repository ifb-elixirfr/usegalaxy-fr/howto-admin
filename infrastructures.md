# Infrastructures

The IFB Core Cluster which hosts [usegalaxy.fr](https://usegalaxy.fr/) provides 3 infrastructures:

|               |                                     | VPN | Default branch |     Who     |
| ------------- | ----------------------------------- | :-: | :------------: | :---------: |
| Development   | https://usegalaxy-dev.dev.ifb.local |  X  |                | Developers  |
| Preproduction | https://usegalaxy.ifb.local/        |  X  |       X        | Maintainers |
| Production    | https://usegalaxy.fr                |     |                | Maintainers |

## VPN access

VPN access is required to access dev and preprod instances.

It's managed by IGBMC, ask Guillaume Seith to get an account.

### VPN config on Fedora

Then "Sous Linux, il faut installer openVPN puis se connecter à l'adresse https://vpn1.igbmc.fr/auth/ puis aller dans "données personnelles". Télécharger le "Profil VPN SSL pour clients OpenVPN" et l’importer dans openvpn."

Unzip the downloaded profile somewhere, then import it:

`nmcli connection import type openvpn file  openvpn_client.ovpn`

Modification of the vpn conf (to access using VPN only for IFB ips):
"paramètres IPv4" -> "Routes..."
Check "Utiliser cette connexion uniquement pour les ressources de son réseau"

To use the IFB DNS:

Create 2 files in /etc/systemd/resolved.conf.d/ (the resolved.conf.d dir might not exist, create it):

- First 01-ifb_preprod.conf containing:

```
[Resolve]
DNS=192.168.103.250
Domains=~ifb.local
```

- Then 02-ifb_dev.conf containing:

```
[Resolve]
DNS=192.168.105.250
Domains=~dev.ifb.local
```

Then reloaded with:

`sudo systemctl restart systemd-resolved`

## Merge requests

### 1. [Developer] Merge request "feature" -> `preprod`

:rocket: The CI launches the tasks on the **development** infrastructure

### 2. [Maintainer] Merge on `preprod`

:rocket: The CI launches the tasks on the **preproduction** infrastructure

### 3. [Maintainer] Merge request `preprod` -> `master`

- Go to https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/merge_requests
- New Merge request
- Source branch : preprod
- Target btanch: master
- Compare branches and continue
- Create Merge request (prefix MR with `Master-` and check `Squash commits when merge request is accepted`.

:rocket: The CI launches the tasks on the **production** infrastructure

[<-- Back](README.md)
