# Add new workflows to test

Go on [workflows tests repository](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/workflows-tests).

The list of workflows to test is contained in `workflows.yml`.

Add workflow with the following pattern :

```yml
name of your test:
  {location (url or local file)}: "wf/url" (if url)
```

example for local worklow:

```yml
Galaxy_metagenomics-amplicon: local
```

Inputs files use to run workflows are stored at `/shared/ifbstor1/bank/galaxy_test-data/workflows`

example for online workflow

```yml
variation-reporting-sars-cov-2:
  url: "https://api.github.com/repos/iwc-workflows/sars-cov-2-variation-reporting/zipball"
```

The url must allow to directly download the workflow to test in zip format.  
For a workflow deposited on github, the url follows the following pattern

```url
https://api.github.com/repos/{user_name}/{repo_name}/zipball/
```

This one will download the last release.  
 If you want a specific release add `refs/tags/{tag_name}` at the end of the url

```url
https://api.github.com/repos/{user_name}/{repo_name}/zipball/refs/tags/{tag_name}
```

[<-- Back](README.md)
