# InterProScan

The InterProScan tool requires some manual action at every update:

- We need to run the corresponding data manager
- We need to have an up-to-date singularity image, including proprietary stuff, e.g. https://gitlab.com/ifb-elixirfr/cluster/tools/-/merge_requests/706
- The sorting hat must be configured to use the built singularity image
- The Galaxy tool runs using the singularity image *but* using the data from the data manager (always mounted in the container). Which means we need to manually copy proprietary data from the singularity image into the data manager directory (e.g. for 5.52-86.0):

```
    on clust-usegalaxy-ubuntu:
    singularity -s exec /shared/ifbstor1/software/singularity/images/interproscan-5.52-86.0.sif /bin/bash
        cp -r /opt/interproscan/data/tmhmm/ /shared/ifbstor1/galaxy/mutable-data/tool-data/interproscan/5.52-86.0/data/
        cp -r /opt/interproscan/data/smart/THRESHOLDS /shared/ifbstor1/galaxy/mutable-data/tool-data/interproscan/5.52-86.0/data/smart/
    # sortir du singularity
    chown -R galaxy:galaxy /shared/ifbstor1/galaxy/mutable-data/tool-data/interproscan/5.52-86.0/data/tmhmm/
    chown -R galaxy:galaxy /shared/ifbstor1/galaxy/mutable-data/tool-data/interproscan/5.52-86.0/data/smart/
```
