# Reference data

How to manage reference data in [usegalaxy.fr](https://usegalaxy.fr/)

## Sources

Currently, depending of the tools, we have 3 sources of reference data:

### Data manager

We have a dedicated gitlab repository with CI for that: https://gitlab.com/ifb-elixirfr/usegalaxy-fr/data_manager

### usegalaxy.org Reference Data

We are mounting the Galaxy project CVMFS shared filesystem: [usegalaxy.org Reference Data](https://galaxyproject.org/admin/reference-data-repo/)

The data repository can be browsed via HTTP at [http://datacache.galaxyproject.or](http://datacache.galaxyproject.org).

A `rsync` launched by `cron` synchronize daily the CVMFS data.galaxyproject.org files on our filesystem.

The corresponding tool_data_table_conf.xml:

- [tool_data_table_conf_cvmfs_managed.xml.j2](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/tool_data_table_conf_cvmfs_managed.xml.j2)
- [tool_data_table_conf_cvmfs_byhand.xml.j2](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/tool_data_table_conf_cvmfs_byhand.xml.j2)

### clust-slurm-client:/shared/bank

We also rely on the banks also available for CLI usage on the IFB Core Cluster: `/shared/bank/`

The corresponding tool_data_table_conf.xml:

- [tool_data_table_conf_local.xml.j2](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/tool_data_table_conf_local.xml.j2)

[<-- Back](README.md)
