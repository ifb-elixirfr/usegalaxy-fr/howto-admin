# TIaas: Training Infrastructure as a Service

## Asking for resources

Users willing to organize a training event on usegalaxy.fr need to fill the form on [https://usegalaxy.fr/tiaas/](https://usegalaxy.fr/tiaas/) ("Apply now!" button).

A mail is sent to contact-usegalaxyfr@groupes.france-bioinformatique.fr.

## Approving a training

After receiving the notification email, an admin of the TIaaS system must go to [https://usegalaxy.fr/tiaas/admin/training/training/](the admin dashboard), click on the new one and check details.

The admin needs to estimate the needed resources based on the description of the training + the available resources from the IFB cluster.

To approve a training request do the following steps:

### Slurm reservation

Make a Slurm reservation for the resources: as root on the Galaxy server, create reservation with a command like this:

```
scontrol create reservation reservationname=training-tiaas_training_id user=galaxy nodecnt=4 StartTime=2022-02-17T11:00 EndTime=2022-02-24T23:30 flags=flex,replace_down
```

The reservation name *MUST* be of the form "training-<tiaas_training_id>", with `tiaas_training_id` = the id of the tiaas request in *lowercase* (even if there are some uppercase letters in https://usegalaxy.fr/tiaas/admin/).

You need to adapt the number of nodes/cores reserved, depending on the content of the training. You can adjust these options:

- `nodecnt`: the number of nodes to reserve
- `nodes`: a list of specific nodes to reserve
- `partitionname`: the partition on which to reserve. Will be the default partition if not specified. If you want to reserve on different partitions (e.g.: `fast` + `docker` for gxit jobs), don't use this option, just list nodes with the `nodes` option
- (`corecnt`: the number of cores to reserve -> this doesn't seem to work)

If you have reserved on a single partition (e.g. `fast`), galaxy will still be able to submit jobs on another partition (e.g. `docker`) without any error.

`StartTime` can be set to `now` in case of emergency.

The `flex` flag allows to run jobs outside the reservation if there is space + launch the jobs before or after the reservation time limit.

The `replace_down` flag allows to automatically reserve a new node if a reserved one goes down for unknown reason.

To see the list of current reservations:

```
scontrol show reservations
```

To remove a reservation:

```
scontrol delete ReservationName=training-xxxx
```

### Approve the training

In [https://usegalaxy.fr/tiaas/admin/training/training/](the admin dashboard), edit the training request, and change its `Processed` attribute to `Approved`, and click on "SAVE".

### Send an email to the training organiser

Send a mail like this to the training organiser (email address is in the training request details). Don't forget to replace `id_of_the_training`!!

```
Hello,

You have asked to organize a training on the UseGalaxy.fr infrastructure. We are delighted to inform you that your request was approved!

In order to make use of the computing resources we have reserved for you, each student of your training will need to:

- Login on https://usegalaxy.fr
- Then visit this url: https://usegalaxy.fr/join-training/<id_of_the_training>/

After this, the students will be able to use Galaxy as usual. All their jobs will automatically use the reserved computing resources.

As a training organiser, you can monitor the jobs of your students by visiting your dashboard: https://usegalaxy.fr/join-training/<id_of_the_training>/status/

In case of problem, feel free to contact us on https://community.france-bioinformatique.fr/c/galaxy/8

Regards,
The UseGalaxy.fr team
```

## How TIaaS works

When a user visits the "join" url, he gets added to a corresponding group/role in Galaxy.

Everytime a job is submitted, the Sorting Hat checks if the user has a `training-*` role. If it's the case, it automatically adds a corresponding the `--reservation=training-*` option to the Slurm native sepcification.

If a user joins multiple training at the same time, the Sorting Hat will pick one of the corresponding reservation randomly.

A cron passes each night to remove obsolete Galaxy roles after the training is finished.
