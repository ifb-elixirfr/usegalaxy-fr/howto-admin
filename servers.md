# Galaxy servers

How to manage the different server components of [usegalaxy.fr](https://usegalaxy.fr/)

## Galaxy instances

|         |                |
| ------- | -------------- |
| prod    | 192.168.16.214 |
| preprod | 192.168.103.82 |
| dev     | 192.168.105.82 |

## Process management

### How it works

Since 22.05, we use Gravity and Systemd to manage Galaxy processes.

There's a global Galaxy Systemd service, which invokes Gravity (`galaxyctl` command), which is responsible for running each processes defined in the galaxy.yml config file.

Graceful restart is performed using unicornherder (will probably change in 23.1)

### Status

```bash
$ systemctl status galaxy
● galaxy.service - Galaxy
     Loaded: loaded (/etc/systemd/system/galaxy.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-11-28 14:58:25 CET; 18h ago
   Main PID: 983789 (galaxyctl)
      Tasks: 467 (limit: 36063)
     Memory: 13.1G (limit: 16.0G)
        CPU: 9h 7min 5.027s
     CGroup: /system.slice/galaxy.service
             ├─ 983789 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/galaxyctl start --foreground --quiet
             ├─ 983803 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/supervisord -c /shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/supervisord.conf --nodaemon
             ├─ 983814 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/unicornherder --pidfile /shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/unicornherder.pid -- galaxy.webapps.galaxy.fast_factory:factory() --timeout 300 --pythonpath lib -k galaxy.webapps.galaxy.workers.Worker -b>
             ├─ 983825 /usr/bin/tail -f /shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/supervisord.log
             ├─1000586 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/gunicorn -D -p /shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/unicornherder.pid galaxy.webapps.galaxy.fast_factory:factory() --timeout 300 --pythonpath lib -k galaxy.webapps.galaxy.workers.Worker -b 0.0.0.0:900>
             ├─1000620 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/gunicorn -D -p /shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/unicornherder.pid galaxy.webapps.galaxy.fast_factory:factory() --timeout 300 --pythonpath lib -k galaxy.webapps.galaxy.workers.Worker -b 0.0.0.0:900>
             ├─1000621 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/gunicorn -D -p /shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/unicornherder.pid galaxy.webapps.galaxy.fast_factory:factory() --timeout 300 --pythonpath lib -k galaxy.webapps.galaxy.workers.Worker -b 0.0.0.0:900>
             ├─1000622 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/gunicorn -D -p /shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/unicornherder.pid galaxy.webapps.galaxy.fast_factory:factory() --timeout 300 --pythonpath lib -k galaxy.webapps.galaxy.workers.Worker -b 0.0.0.0:900>
             ├─1000623 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/gunicorn -D -p /shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/unicornherder.pid galaxy.webapps.galaxy.fast_factory:factory() --timeout 300 --pythonpath lib -k galaxy.webapps.galaxy.workers.Worker -b 0.0.0.0:900>
             ├─1000624 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/gunicorn -D -p /shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/unicornherder.pid galaxy.webapps.galaxy.fast_factory:factory() --timeout 300 --pythonpath lib -k galaxy.webapps.galaxy.workers.Worker -b 0.0.0.0:900>
             ├─1000630 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/celery --app galaxy.celery worker --concurrency 2 --loglevel DEBUG --pool threads --queues celery,galaxy.internal,galaxy.external
             ├─1000792 /shared/ifbstor1/galaxy/.venv/bin/python -c from multiprocessing.semaphore_tracker import main;main(9)
             ├─1000797 /shared/ifbstor1/galaxy/.venv/bin/python /shared/ifbstor1/galaxy/.venv/bin/celery --app galaxy.celery beat --loglevel DEBUG --schedule /shared/ifbstor1/galaxy/mutable-data/gravity/celery-beat-schedule
             ├─1000896 /usr/local/sbin/tusd -host=192.168.16.214 -port=1080 -upload-dir=/shared/ifbstor1/galaxy/tmp_upload/ -hooks-http=https://usegalaxy.fr/api/upload/hooks -hooks-http-forward-headers=X-Api-Key,Cookie -hooks-enabled-events pre-create
             ├─1001133 python ./lib/galaxy/main.py -c /shared/ifbstor1/galaxy/config/galaxy.yml --server-name=handler_0 --attach-to-pool=job-handlers --attach-to-pool=workflow-schedulers --pid-file=/shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/handler_0.pid
             ├─1001209 python ./lib/galaxy/main.py -c /shared/ifbstor1/galaxy/config/galaxy.yml --server-name=handler_1 --attach-to-pool=job-handlers --attach-to-pool=workflow-schedulers --pid-file=/shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/handler_1.pid
             ├─1001394 python ./lib/galaxy/main.py -c /shared/ifbstor1/galaxy/config/galaxy.yml --server-name=handler_2 --attach-to-pool=job-handlers --attach-to-pool=workflow-schedulers --pid-file=/shared/ifbstor1/galaxy/mutable-data/gravity/supervisor/handler_2.pid
             ├─1130074 npm exec gx-it-proxy --ip 0.0.0.0 --port 8000 --sessions /shared/ifbstor1/galaxy/mutable-data/interactivetools_map.sqlite --verbose
             ├─1130097 sh -c gx-it-proxy "--ip" "0.0.0.0" "--port" "8000" "--sessions" "/shared/ifbstor1/galaxy/mutable-data/interactivetools_map.sqlite" "--verbose"
             └─1130098 node /shared/ifbstor1/galaxy/.npm/_npx/9c941384427606c2/node_modules/.bin/gx-it-proxy --ip 0.0.0.0 --port 8000 --sessions /shared/ifbstor1/galaxy/mutable-data/interactivetools_map.sqlite --verbose
```

### Start/Stop

Using Systemd:

```bash
$ systemctl start galaxy
$ systemctl stop galaxy
```

It will take some time to launch all the processes

### Restart gracefully

```bash
$ systemctl reload galaxy
```

Gravity will restart all the processes one by one (including handlers). Which means it will take some time.

The web processes are handled differently: Gravity signals Unicornherder, which is responsible for starting new processes, then killing the old ones when the new ones are ready.

### Restart *not* gracefully

There is a way to kill everything then restart everything (which means downtime):

```bash
$ systemctl restart galaxy
```

### Restart single services

It is possible to run Gravity commands to have control on indidual processes. For example to start the gx_it_proxy process in production (adapt paths for dev/preprod):

```bash
export GRAVITY_STATE_DIR=/shared/galaxy/mutable-data/gravity/
$ /shared/galaxy/.venv/bin/galaxyctl start gx-it-proxy
```

### Restart all services: usegalaxy, database, and nginx

#### Stop all services in the following order

Note that all 3 services are on separate machines. Check the IP on this page.

```bash
# Stop nginx
systemctl stop nginx
# Stop usegalaxy en premier
systemctl stop galaxy
# Stop postgres
systemctl stop postgresql-11.service
```

#### Start all services in the following order

```bash
# Start postgres
systemctl start postgresql-11.service
# Start usegalaxy
systemctl start galaxy
# Start nginx
systemctl start nginx
```

## galaxy_db

|         |                 |
| ------- | --------------- |
| prod    | 192.168.16.213  |
| preprod | 192.168.103.120 |
| dev     | 192.168.105.120 |

## galaxy_nginx

|         |                 |                                     |
| ------- | --------------- | ----------------------------------- |
| prod    | 192.168.16.204  | https://usegalaxy.fr                |
| preprod | 192.168.103.105 | https://usegalaxy.ifb.local/        |
| dev     | 192.168.105.105 | https://usegalaxy-dev.dev.ifb.local |

[<-- Back](README.md)
