# Start

## amqp - `ConnectionRefusedError: [Errno 111] Connection refused`

### Explaination

RabbitMQ isn't started properly

### Check / Solution

1- The service Docker isn't started 

## Restart stuck at `galaxy.tool_util.deps.containers DEBUG [...] Loading container resolution config from file '[...]container_resolvers_conf.yml'

### Explaination

Galaxy is listing a cache folder within the cvmfs mount: `cache_directory: "/cvmfs/singularity.galaxyproject.org/all/`

### Check / Solution

1- Check if galaxy can list the contain of `ls /cvmfs/singularity.galaxyproject.org/all/`
2- If not: `systemctl restart autofs`
3- `cvmfs_config umount` then `ls cvmfs/singularity.galaxyproject.org cvmfs/data.galaxyproject.org` to mount again