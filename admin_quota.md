# How to admin quota

1. [Create group](#group)
2. [Create quota](#quota)
3. [Change user quota](#user)

<a name="group"></a>

## 1. Create group

1. Log in as admin on usegalaxy.fr
2. Go to the Admin section in the top bar
3. Select "Group" in User Management
4. Click on "Add new group" at the top right of the window
5. Fill as needed
6. Save

<a name="quota"></a>

## 2. Create quota

1. Log in as admin on usegalaxy.fr
2. Go to the Admin section in the top bar
3. Select "Quotas" in User Management
4. Click on "Add new quota" at the top right of the window
5. Fill as needed (It is mandatory to associate the quota with a group)
6. Save

<a name="user"></a>

## 3. Change user quota

1. Log in as an admin on usegalaxy.fr
2. Go to the Admin section in the top bar
3. Select "Users" in User Management
4. Search for the user
5. Right click then Manage Roles and Groups
6. Add the desired quota group
7. Save

[<-- Back](README.md)
