# How to administer [usegalaxy.fr](https://usegalaxy.fr/)

## Index

- [Infrastructure Details](infrastructures.md)
- [Servers Details](servers.md)
- [Tools Installation](tools.md)
- [Reference Data Details](reference_data.md)
- [Add new workflows to test](add_wf_tests.md)
- [Admin quota](admin_quota.md)
- [TIaaS: Training Infrastructure as a Service](tiaas.md)

## 3 Infrastructures

- Development instance: https://usegalaxy-dev.dev.ifb.local
- Preproduction instance: https://usegalaxy.ifb.local/
- Production instance: https://usegalaxy.fr
