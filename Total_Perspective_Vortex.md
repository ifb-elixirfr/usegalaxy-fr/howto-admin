# Fonctionnement général

Le **Total Perspective Vortex (TPV)** est un système de gestion dynamique des outils, destinations, et utilisateurices dans Galaxy.
Il repose sur des fichiers de configuration qui définissent les règles de mappage entre les outils et les destinations, ainsi que sur un mécanisme pour copier et mettre à jour ces fichiers.

## 1. Localisation des fichiers de configuration

Sur le serveur, les fichiers du TPV sont stockés dans deux endroits principaux :

### 1. Répertoire mutable-data géré par Ansible

- Chemin : `galaxy/mutable-data/total_perspective_vortex/*`
- **Rôle :** Ce dossier est la source initiale où les fichiers sont copiés par la **CI Ansible**.
- Lors du démarrage ou redémarrage de Galaxy, un script nommé `tpv-lint-and-copy.sh` copie les fichiers depuis ce répertoire  vers le répertoire de configuration `config`.

### 2. Répertoire de configuration utilisé par Galaxy

- Chemin : `galaxy/config/total_perspective_vortex/*`
- **Rôle :** C’est ici que Galaxy accède aux fichiers du TPV pour appliquer ses règles.

### 3. Les fichiers

### Fichiers de configuration du TPV

1. `tool_defaults.yml` : Définit les valeurs par défaut des outils.  
  [documentation](tpv-files/tool_default.md)
2. `tools.yml` : Associe des outils spécifiques à des partitions ou ressources dédiées.  
  [documentation](tpv-files/tools.md)
3. `destinations.yml` : Configure les destinations de calcul et leurs règles.  
  [documentation](tpv-files/destination.md)
4. `interactive_tools.yml` : Gère les outils interactifs.  
  [documentation](tpv-files/interactive_tools.md)
5. `users.yml` : Définit les groupes et permissions des utilisateurices.  
  [documentation](tpv-files/users.md)

## 2. Fonctionnement dynamique du TPV

### 1. Application des configurations

- Après leur copie dans `galaxy/config/total_perspective_vortex`, les fichiers sont utilisés par Galaxy pour mapper dynamiquement les outils aux destinations définies dans `tools.yml`.

### 2. Modification à la volée

- Si une modification manuelle est nécessaire, elle peut être effectuée directement dans `galaxy/config/total_perspective_vortex/*`.
- Ces changements prennent effet immédiatement (sans nécessiter de redémarrage), ce qui est utile pour des ajustements rapides ou des tests (en dev et preprod).
- /!\ Il peut arriver que le système se bloque. Dans ces cas la, il faut supprimer les fichiers présents dans ce répertoire et redémarrer/recharger le service galaxy.

### 3. Vérification des logs

- Les résultats et comportements liés aux fichiers TPV peuvent être observés dans les fichiers de logs Galaxy.
- Chemin des logs : `galaxy/var/log/handler_*.log`.

```bash
tpv.rules.gateway INFO 2024-12-03 17:46:32,330 [pN:handler_2,p:1106044,tN:JobHandlerQueue.monitor_thread] reloading tpv rules from: ['https://raw.githubusercontent.com/galaxyproject/tpv-shared-database/main/tools.yml', '/shared/nfs/data/galaxy/config/total_perspective_vortex/tool_defaults.yml', '/shared/nfs/data/galaxy/config/total_perspective_vortex/tools.yml', '/shared/nfs/data/galaxy/config/total_perspective_vortex/destinations.yml', '/shared/nfs/data/galaxy/config/total_perspective_vortex/interactive_tools.yml', '/shared/nfs/data/galaxy/config/total_perspective_vortex/users.yml']
```

- exemple après avoir changer la mémoire par défaut dans tool_default.yml
  
## 3. Ajout des fichiers de configuration du TPV via Ansible

Dans le fichier `group_vars/galaxy/main.yml`du repository [infrastructure](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure) , il est nécessaire de faire la  copie des fichiers de configuration.

```yaml
galaxy_config_files:
    - src: "{{ galaxy_tpv_src_dir }}/users.yml"
      dest: "{{ tpv_mutable_dir }}/users.yml"
```

## 4. Ajout de la destination TPV dans job_conf.yml

Le fichier job_conf.yml configure la destination des jobs dans l'environnement Galaxy. Le système TPV utilise un type de "runner" dynamique, qui attribue les outils aux bonnes destinations en fonction des règles définies.

```yaml
default_destination: tpv_dispatcher
destinations:
  - id: local
    runner: local_runner
  - id: tpv_dispatcher
    runner: dynamic
    type: python
    function: map_tool_to_destination
    rules_module: tpv.rules
    tpv_config_files:
      - "https://raw.githubusercontent.com/galaxyproject/tpv-shared-database/main/tools.yml"
      - "{{ galaxy_config_dir }}/total_perspective_vortex/tools.yml"
      - "{{ galaxy_config_dir }}/total_perspective_vortex/tool_defaults.yml"
      - "{{ galaxy_config_dir }}/total_perspective_vortex/destinations.yml"
      - "{{ galaxy_config_dir }}/total_perspective_vortex/interactive_tools.yml"
      - "{{ galaxy_config_dir }}/total_perspective_vortex/users.yml"
```

- rules_module : tpv.rules
  - Ces paramètres indiquent que le système TPV utilise un gestionnaire dynamique en Python pour déterminer la destination des outils. La fonction map_tool_to_destination est utilisée pour attribuer les outils aux destinations appropriées en fonction des règles définies dans le module tpv.rules.

- `tpv_config_files` : Cette liste contient les fichiers de configuration utilisés par le TPV pour gérer les outils Galaxy. Ces fichiers doivent être accessibles et utilisés par le système TPV pour assigner correctement les outils aux bonnes destinations.
Les fichiers sont lus dans l'ordre dans lequel ils sont listés dans tpv_config_files. Cela signifie que :
  - Si une information est trouvée dans le fichier en premier dans la liste, elle sera utilisée.
  - Si une information n'est pas trouvée, le système passera au fichier suivant dans la liste.
