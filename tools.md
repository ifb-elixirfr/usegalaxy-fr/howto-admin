# Tools

How to install and manage tools in [usegalaxy.fr](https://usegalaxy.fr/)

## Install a Tool

**Check list:**

1. If a new section have to be created, first of all add it in [tool_conf.xml](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/tool_conf.xml)
2. If a new section have been created, and if it is specific to one of the subdomains, add it here [global_host_filters.py.j2](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/templates/galaxy/global_host_filters.py.j2)

### .yml and .yml.lock

The `.yml` is the same as the `*.yml.lock` without the revision numbers. `*.yml.lock` files are automatically updated, you should not need to modify them.

### Update one or severals tools using the CLI

The script [update-tool.py](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/tools/-/blob/preprod/scripts/update-tool.py) allow to update all the tools of a `.yml` file in the `.yml.lock` file.

**Installation:**
Using [miniconda](https://docs.conda.io/en/latest/miniconda.html)

```bash
conda create -n bioblend bioblend
```

**Usage:**

```bash
conda activate bioblend
python script/update-tool.py files/workflow4metabolomics.yml
git add files/workflow4metabolomics.yml.lock
git commit -m "files/workflow4metabolomics.yml.lock - update all the w4m tools"
```

```bash
conda activate bioblend
for i in files/*.yml; do python ./scripts/update-tool.py --owner iuc $i; done
```

### Dependencies

By default [usegalaxy.fr](https://usegalaxy.fr/) is using Conda packages to resolve the wrappers dependencies.

But it's also compatible with [Singularity](https://sylabs.io/docs/) containers, pulled automatically or not from [Biocontainers](https://biocontainers.pro/#/).

## Computer resources available for tools

We use the destinations used on usegalaxy.eu as a basis: [usegalaxy-fr/tool_destinations.yaml](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/production/files/dynamic_rules/tool_destinations.yaml)

By default

```yaml
tool_name: { cores: [int], mem: [int] }
```

ex :

```yaml
bwa: { cores: 8, mem: 20 }
```

We can customize the runner

```yaml
tool_name:
  { cores: [int], mem: [int], runner: runner_name, force_destination_id: True }
```

Runners are defined in [usegalaxy-fr/destination_spcifications.yaml](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/preprod/production/files/dynamic_rules/destination_specifications.yaml)

```yaml
runner_name:
  info:
    remote: "False"
    scheduler: "slurm"
  limits:
    # maximum resources that a single job can ask
    cores: [int]
    mem: [int]
  env:
    GALAXY_MEMORY_MB: "{MEMORY_MB}"
    GALAXY_SLOTS: "{PARALLELISATION}"
  params:
    nativeSpecification: "-p fast -c {PARALLELISATION} --mem={MEMORY_MB} {NATIVE_SPEC_EXTRA}"
    tmp_dir: "True"
```

[usegalaxy.fr](usegalaxy.fr/) is implementing the `resubmit` system if the tool reach its memory limit. We used to apply a factor of 1.5 for the second destination.

### The Helpful Script

To help us, especially if we need to insert a bunch of tools, we have this little utils: [job_conf_helper.sh](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/infrastructure/-/blob/master/scripts/job_conf_helper.sh)

**Usage:**

```bash
# For a single tool
bash job_conf_helper.sh mspurity_spectralmatching 4 8

# For a suite of tools - we can split the 2 sections to ease the copy/paste
for tool in mspurity_spectralmatching mspurity_flagremove mspurity_purityx; do bash job_conf_helper.sh $tool 4 8 destinations; done
for tool in mspurity_spectralmatching mspurity_flagremove mspurity_purityx; do bash job_conf_helper.sh $tool 4 8 tools; done
```

:warning: Note that so far, we dont implement any linting so becareful :)

[<-- Back](README.md)
